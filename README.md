##Simple Mail Scheduler 

According to the task 01, this simple script includes a mail scheduler based on Node.js.

This includes;

    express
    mongoose
    react
    react-dom
    react-router-dom
    bootstart
    parcel
 
 **Install dependencies**
 
    npm intall
 
 **Start**
 
 Make sure mongo DB is running on localhost 27017.
 
    npm start

**Dev**

    npm dev

###Features

* Sending Emails
* Email queue 
* Set priority
* Scheduled emails
* Unicast, Multicast, Broadcast
